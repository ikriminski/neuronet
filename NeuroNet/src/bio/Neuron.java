package bio;

import java.util.ArrayList;


public class Neuron {

	ArrayList<Integer> connection;
	BioNet net;
	boolean active;
	int cooldown;
	boolean output;
	String outputString;
	int threshold;
	int potential;
	int firingPotential;
	int numTimeHit;
	/*
	 * Creates a new neuron with a reference to the network that is hosting it.
	 */
	public Neuron( BioNet bioNet){
		active = false;
		net = bioNet;
		connection = new ArrayList<Integer>();
		cooldown = 0;
		output = false;
		threshold = 0;
		potential = 0;
	}
	/*
	 * Adds a connection to another neuron.
	 */
	public void addConnection(int connection){
		this.connection.add(connection);
	}
	/*
	 * This method is triggered if a neuron was successfully set as active in the previous
	 * BioNet step.
	 * The current neuron propagates its signal (firingPotential) to all connected neurons,
	 * Then the neuron becomes inactive.
	 * If it is an output neuron, output is added to the outputbuffer of the BioNet
	 */
	public void fire() {
		for(int i : connection){
			if(net.neurons.size()>i)
				net.neurons.get(i).setActive(firingPotential);
			//System.out.println("fire");
		}
		if(output){
			net.gatherOutput(outputString);
			//System.out.println("gaveoutput");
		}
		active = false;		
		net.removeActiveNeuron(this);
		
	}
	/*
	 * If signal strength is sufficient, this neuron becomes active.
	 */
	public void setActive(int firingPower) {
		potential += firingPower;
		if(cooldown <= 0 && potential >= threshold){
			active = true;
			net.addActiveNeuron(this);
			cooldown = 30;
		}
	}
	/*
	 * Decrements the cooldown. And reduces built up potential.
	 */
	public void cooldown() {
		cooldown--;
		potential = potential/4;
		
	}
	public void setThreshold(int threshold){
		this.threshold = threshold;
	}
	public void setAsOutput(String outputString){
		output = true;
		this.outputString = outputString;
	}
	public void setFiringPotential(int value) {
		firingPotential = value;
		
	}
}
