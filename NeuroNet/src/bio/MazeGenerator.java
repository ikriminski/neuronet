package bio;

public class MazeGenerator {
	/*
	 * Generates an obstacle course. Used for testing.
	 */
	public boolean[][] generateMaze(int x, int y){
		boolean[][] maze = new boolean[x][y];
		for(int i = 0; i< x; i++){
			for (int j = 0; j<y; j++){
				if(Math.random() > 0.8)
					maze[x][y] = true;
			}
		}
		return maze;
		
	}

}
