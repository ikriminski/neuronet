package bio;

public class Codon {
	/*
	 * A codon is a basic unit of network architecture information
	 */
	public static enum TYPE {CONNECTION, NEURON, THRESHOLD, POTENTIAL};
	public TYPE type;
	public int value;
	public int value2;
	public Codon(Codon.TYPE type, int value, int value2){
		this.type = type;
		this.value = value;
		this.value2 = value2;
	}

	public Codon copy(){
		return new Codon(type, value, value2);
	}

	@Override
	public String toString() {
		return "Codon [type=" + type + ", value=" + value + ", value2="
				+ value2 + "]";
	}
	
}
