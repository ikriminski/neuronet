package bio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class BioNet {
	ArrayList<Codon> DNA;
	public ArrayList<Neuron> neurons;
	ArrayList<Neuron> activeNeurons;
	ArrayList<Neuron> neuronsToAdd;
	ArrayList<Neuron> neuronsToRemove;
	public ArrayList<Neuron> inputNeurons;
	public ArrayList<Neuron> outputNeurons;
	public String outputBuffer;
	/*
	 * Instantializes a BioNet given a DNA arraylist.
	 * It is necessary to call buildNet before running.
	 */
	public BioNet(ArrayList<Codon> dna){
		DNA = dna;
		neurons = new ArrayList<Neuron>();
		neuronsToAdd = new ArrayList<Neuron>();
		activeNeurons = new ArrayList<Neuron>();
		neuronsToRemove = new ArrayList<Neuron>();
		inputNeurons = new ArrayList<Neuron>();
		outputNeurons = new ArrayList<Neuron>();
		outputBuffer = "";
	}
	/*
	 * Creates neurons and connections within network.
	 */
	public void buildNet(){
		for(Codon c : DNA){
			if(c.type == Codon.TYPE.NEURON){
				neurons.add(new Neuron(this));					
				}
			if(c.type == Codon.TYPE.CONNECTION){
				if(c.value >= neurons.size() || c.value2 >= neurons.size()){
					System.out.println("Connection out of bounds");
				} else {
					neurons.get(c.value).addConnection(c.value2);
				}
			}				
			if(c.type == Codon.TYPE.POTENTIAL){
				if(c.value >= neurons.size()){
					System.out.println("Neuron out of bounds");
				} else {
					neurons.get(c.value).setFiringPotential(c.value2);
				}
			}
			if(c.type == Codon.TYPE.THRESHOLD){
				if(c.value >= neurons.size()){
					System.out.println("Neuron out of bounds");
				} else {
					neurons.get(c.value).setThreshold(c.value2);
				}
			}
		}		
		//System.out.println("done building");
	}
	/*
	 * Applies random mutations to the DNA and return modified DNA.
	 * Does not change the current structure.
	 */
	public ArrayList<Codon> mutate(){
		ArrayList<Codon> newDNA = new ArrayList<Codon>();
		for(Codon c : DNA){
			newDNA.add(c.copy());
		}
		int neuronCount = neurons.size()-1;
		for(int i = 0; i< 4;i++) {
			double rand = Math.random();
			Codon c;
			if(rand < 0.2){ //new connection
				int newValue = (int) (Math.random()*neuronCount);
				int newValue2 = (int) (Math.random()*neuronCount);
				if(newValue != newValue2){
					c = new Codon(Codon.TYPE.CONNECTION, newValue, newValue2);
					newDNA.add(c);
				}
				
			} else {
				if (rand < 0.5){// new neuron
					c = new Codon(Codon.TYPE.NEURON, 0 , 0);
					newDNA.add(c);
					neuronCount++;
					int newValue = (int) (Math.random()*(neuronCount));
					c = new Codon(Codon.TYPE.CONNECTION, newValue, neuronCount);
					newDNA.add(c);
					newValue = (int) (Math.random()*(neuronCount));
					c = new Codon(Codon.TYPE.CONNECTION, neuronCount, newValue);
					newDNA.add(c);
				} else {
					if (rand < 0.75) { //threshold
						int newValue = (int) (Math.random()*neuronCount);
						int newValue2 = (int) (Math.random()*5) +1;
						c = new Codon(Codon.TYPE.THRESHOLD, newValue, newValue2);
						newDNA.add(c);
					} else { //potential
						int newValue = (int) (Math.random()*neuronCount);
						int newValue2 = (int) (Math.random()*5);
						c = new Codon(Codon.TYPE.POTENTIAL, newValue, newValue2);
						newDNA.add(c);
					}
				}
			}
		}
		return newDNA;

	}
	@Override
	public String toString() {
		String out = "";
		for(Codon c: DNA){
			out += c.toString() + "++";
		}
		return out;
	}
	/*
	 * Performs one computational step
	 */
	public void step(){
		activeNeurons.removeAll(neuronsToRemove);
		neuronsToRemove.clear();
		for(Neuron a : activeNeurons){
			a.fire();
			//System.out.println("fire");
		}
		activeNeurons.addAll(neuronsToAdd);
		neuronsToAdd.clear();
		cooldown();
	}
	/*
	 * Coolsdown all neurons. Cooldown was implemented to remove infinite loops within
	 * network.
	 */
	private void cooldown() {
		for(Neuron n : neurons){
			n.cooldown();
		}

	}
	public void removeActiveNeuron(Neuron n){
		neuronsToRemove.add(n);
	}
	public void addActiveNeuron(Neuron n){
		neuronsToAdd.add(n);
	}
	/*
	 * Sets a neuron to be output. 
	 */
	public void setOutputNeuron(Neuron n, String output){
		n.setAsOutput(output);
	}
	/*
	 * Sets a neuron to be output.
	 */
	public Neuron setOutputNeuron(int location, String output){
		Neuron currentNeuron = neurons.get(location);
		currentNeuron.setAsOutput(output);
		return currentNeuron;
	}
	/*
	 * Sets multiple neurons to be output neurons.
	 * Call after createInputArray()
	 */
	public void createOutputArray(int size, String[] output){
		//System.out.println(neurons.size());
		for(int i = 0; i < size; i++ ){
			//System.out.println("i:" + i + inputNeurons.size());
			Neuron currentNeuron = setOutputNeuron(i + inputNeurons.size(), output[i]);
			outputNeurons.add(currentNeuron);
		}
	}
	/*
	 * Sets the first size number of neurons as input neurons.
	 */
	public void createInputArray(int size){
		for(int i = 0; i < size; i++){
			inputNeurons.add(neurons.get(i));
		}
	}
	/*
	 * Append output to buffer
	 */
	public void gatherOutput(String outputString) {
		outputBuffer += outputString;
		
	}
	/*
	 * Flush buffer
	 */
	public void flushOutputBuffer(){
		outputBuffer = "";
	}
	
	public String readOutputBuffer(){
		return outputBuffer;
	}
	
	public boolean hasActiveNeurons(){
		return activeNeurons.size()  > 0;
	}
	
	public ArrayList<Codon> getDNA(){
		return DNA;
	}
	/*
	 * Resets cooldowns on all neurons.
	 */
	public void reset() {
		for(int i = 0; i< 30; i++){
			cooldown();
		}
		
	}
}
