package main;

import java.io.Serializable;

public class Input implements Serializable {
	float in;
	Node parent;
	/*
	 * This class creates a handle to input nodes within the network.
	 */
	public void setParent(Node n){
		parent = n;
	}
	
	public void setInput(float x){
		in = x;
	}
	public void recieveInput(float x){
		in +=x;
		parent.makeActive();
	}
	public void print(){
		System.out.println("parent:" + parent.id);
		System.out.println(in);
	}
	
	public float getInput(){
		return in;
	}
	public void step(){
		in--;
		if(in < 0){
			in = 0;
		}
	}
	public void zero(){
		in = 0;
	}
}
