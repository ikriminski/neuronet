package main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Network implements Serializable {
	/*
	 * This method links two layers within network
	 */
	public static void linkLayers(ArrayList<Node> prev, ArrayList<Node> cur){
		for(Node n: prev){
			n.setAdjacent(cur);
		}
	}
	/*
	 * Return an input object with no parent
	 */
	public static Input makeInput(){
		Input in = new Input();
		in.setInput(0);
		return in;
	}
	/*
	 * Creates a new layer.
	 */
	public static ArrayList<Node> makeLayer(int size, int nextSize, int layerNum, Network net){
		ArrayList<Node> ret = new ArrayList<Node>(size);
		for(int i = 0; i< size; i++){
			ret.add(makeNode(nextSize, layerNum + "-" + i, net));
		}
		return ret;
	}
	/*
	 * Factory method that creates a new Network. Needs a reference to the runner.
	 */
	public static Network makeNetwork(int inSize, int outSize, int numLayers, Runner run){
		Network net = new Network();
		net.active = new ArrayList<Node>();
		net.run = run;
		net.netOut = new float[outSize];
		net.layers = new ArrayList<ArrayList<Node>>();
		net.events = new Stack<Event>();
		net.inputNodes = makeLayer(inSize, (inSize + (int)( ((float) outSize - (float) inSize) / (float) numLayers )), 0, net);
		net.outputNodes = makeOutLayer(outSize, numLayers -1, net);
		net.layers.add(net.inputNodes);
		int nextSize;
		for(int i = 1; i < numLayers -1; i++){
			if(i == numLayers - 2){
				nextSize = net.outputNodes.size();
			} else {
				nextSize = inSize + (int)((( (float) outSize - inSize) / (numLayers-1))*(i+1));
			}
			int currentSize = inSize + (int)((( (float) outSize - inSize) / (numLayers-1))*(i));
			System.out.println("cur:" + currentSize + " next:" + nextSize);
			net.layers.add(makeLayer(currentSize, nextSize, i, net));
		}
		net.layers.add(net.outputNodes);
		for(int i = 1; i< net.layers.size(); i ++){
			linkLayers(net.layers.get(i-1), net.layers.get(i));
		}
		return net;
	}
	public static Node makeNode(Input in, Output out){
		Node ret = new Node();
		ret.setInput(in);
		in.setParent(ret);
		ret.setOutput(out);
		out.setParent(ret);
		return ret;
	}
	public static Node makeNode(int size){
		Input in = makeInput();
		Output out = makeOutput(size);
		return makeNode(in, out);
	}
	/*
	 * Creates a new node
	 */
	public static Node makeNode(int size, String id, Network net){
		Node ret = makeNode(size);
		ret.setId(id);
		ret.setNetwork(net);
		ret.certaintyf = 0.2f;
		ret.certaintys = 0.2f;
		ret.firingpotential = 1f;
		ret.stallpotential = 1f;
		ret.threshold = 0f;
		ret.certaintyt = 0.8f;
		return ret;
	}
	/*
	 * Creates an output layer
	 */
	public static ArrayList<Node> makeOutLayer(int size, int layerNum, Network net){
		ArrayList<Node> ret = new ArrayList<Node>(size);
		for(int i = 0; i< size; i++){
			ret.add(makeOutNode(layerNum + "-" + i, net));
		}
		return ret;
	}
	/*
	 * Creates an output node.
	 */
	public static OutNode makeOutNode(String id, Network n){
		OutNode out = new OutNode();
		out.setInput(makeInput());
		out.in.setParent(out);
		out.setNetwork(n);
		out.setId(id);
		return out;
	}
	/*
	 * Creates an output array that can be added to layer or node.
	 */
	public static Output makeOutput(int size){
		float x = (float) (1.0/size);
		ArrayList<Float> P = new ArrayList<Float>();
		for(int i = 0; i< size; i++){
			P.add(x*(i+1));
		}
		Output out = new Output();
		out.setP(P);
		return out;
	}
	public ArrayList<ArrayList<Node>> layers;
	public ArrayList<Node> inputNodes;
	public ArrayList<Node> outputNodes;
	public ArrayList<Node> active;
	public float[] netOut;
	public Stack<Event> events;
	public transient Runner run;
	public void addActive(Node node) {
		active.add(node);
	}
	public void flushLog(){
		events.clear();
	}
	public void flushOutput(){
		netOut = new float[netOut.length];
	}
	public boolean hasActive(){
		if(!active.isEmpty())
			return true;
		return false;
	}
	public void log(Event e){
		events.push(e);
	}
	/*
	 * Loosens the network, increasing signal permeability.
	 */
	public void loosenNetwork(float lambda){
		for(ArrayList<Node> l : layers){
			for(Node n : l){
				n.loosen(lambda);
			}
		}
	}
	public void print(){
		for(ArrayList<Node> list : layers){
			for(Node n : list){
				System.out.print(n.id + ", ");
			}
			System.out.println();
		}
	}
	/*
	 * Adds output to log.
	 */
	public void readOutput(OutNode n, float in){
		int index = outputNodes.indexOf(n);
		netOut[index] += in;
	}
	
	public void removeActive(Node node) {
		active.remove(node);		
	}
	public void setInput(float[] in){
		for(int i = 0; i< in.length; i++){
			inputNodes.get(i).in.recieveInput(in[i]);
		}
	}
	
	public void setRunner(Runner r){
		run = r;
	}
	/*
	 * One computation step.
	 */
	public boolean step(){	
		run.askForInput();
		ArrayList<Node> nactive = new ArrayList<Node>(active);
		for(Node n: nactive){
			n.readInput();
		}
		return hasActive();
	}
	/*
	 * Based on actions performed by neurons during testing,
	 * this method updates internal parameters of neurons.
	 */
	public void teach(float lambda){
		for(Event e : events){
			e.getNode().modifyThreshold(e.getInput(), lambda);
			e.getNode().modifyOutput(e.getTarget(), lambda/2);
			e.getNode().modifyFiringPotential(e.getTarget().length, lambda);
			e.getNode().modifyStallPotential(e.getStall(), lambda);
		}
	}
	
	public void teachA(float lambda){
		for(Event e: events){
			e.getNode().modifyOutput(e.getTarget(), lambda);
		}
	}

	public void teachP(float lambda){
		for(Event e : events){
			e.getNode().modifyThreshold(e.getInput(), lambda);
			//e.getNode().modifyOutput(e.getTarget(), lambda);
			e.getNode().modifyFiringPotential(e.getTarget().length, lambda);
			e.getNode().modifyStallPotential(e.getStall(), lambda);
		}
	}
	@Override
	public String toString() {
		return "Network [layers=" + layers + ", netOut=" + Arrays.toString(netOut) + "]";
	}
}
