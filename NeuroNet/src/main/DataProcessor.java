package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DataProcessor {
	/*
	 * This class loads data and preprocesses it.
	 */
	private int[][] data;
	public int[][][] yearData;
	public int[][] processFiles(String[] filenames, int tickSize, int testTickSize, int startDate, int duration){
		Scanner scn = null;
		File file;
		data = new int[duration + 1][filenames.length];
		for(int i = 0; i<filenames.length; i++){
			file = new File(filenames[i]);
			try {
				scn = new Scanner(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String line;
			String[] splitline;
			String[] date;
			boolean foundStart = false;
			while (scn.hasNext() && !foundStart){
				line = scn.nextLine();
				splitline = line.split(";");
				date = splitline[0].split(" ");
				if(Integer.parseInt(date[0]) == startDate){
					foundStart = true;
				} else {
					if(Integer.parseInt(date[0]) > startDate){
						return null;
					}
				}
			}
			for(int j = 0; j<duration; j++){
				splitline = scn.nextLine().split(";");
				data[j][i] = (int) (Float.parseFloat(splitline[1])*1000);
				for(int k = 0; k < tickSize -1 ; k++){
					scn.nextLine();
				}
			}
			//System.out.println(scn.nextLine());
			for(int j = 0; j<testTickSize; j++){
				scn.nextLine();
			}
			String nextL = scn.nextLine();
			//System.out.println(nextL);
			int testdata = (int) (Float.parseFloat(nextL.split(";")[1])*1000);
			data[duration][i]= testdata;
		}
		return data;
	}
	public int[][][] processYearData(String[] filenames, int tickSize, int testTickSize, int duration){
		yearData = new int[370][][];
		for(int m = 0; m< 12; m++){
			for(int d = 0; d<31; d++){
				System.out.println(m*31 + d);
				if(m*31 + d == 370)
					break;
				yearData[m*31 + d] = processFiles(filenames, tickSize, testTickSize, 20130000 + 100 * (m+1) + d + 1, duration);
			}
		}
		return yearData;
	}
}
