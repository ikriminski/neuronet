package main;

import java.io.Serializable;

public class OutNode extends Node implements Serializable {
	public OutNode(){
		out = new Output();
	}
	public void fire(int num){
		net.readOutput(this, 1);
	}
	
}
