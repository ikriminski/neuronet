package main;

import java.io.Serializable;

public class Event implements Serializable {
	Node n;
	float input;
	int[] targets;
	int stall;
	/*
	 * This class creates an event which stores info about how the network has been executing.
	 * Later, this information is used for updating weights during training.
	 */
	public Event(int curStall){
		stall = curStall;
	}
	public Event(Node n, float input, int[] target){
		this.n = n;
		this.input = input;
		this.targets = target;
	}
	public Node getNode(){
		return n;
	}
	public float getInput(){
		return input;
	}
	public int[] getTarget(){
		return targets;
	}
	public int getStall(){
		return stall;
	}
	public String toString(){
		String ret = n.id + "- input:" + input + " stall:" + stall + " targets:";
		for(int i = 0; i< targets.length; i++){
			ret += targets[i] + ", ";
		}
		return ret;
	}
}
