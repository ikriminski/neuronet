package main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Output implements Serializable {
	/*
	 * This is a representation of the output of a node.
	 */
	Node parent;
	ArrayList<Float> P;
	public Output(){
		P = new ArrayList<Float>();
	}
	public void setParent(Node n){
		parent = n;
	}

	public void setP(ArrayList<Float> N){
		for (float f : N){
			P.add(f);
		}
	}
	public void print(){
		//System.out.println("parent:" + parent.id);
		for (float f : P){
			System.out.print(f + ", ");
		}
		System.out.println();
	}
	public int pickTarget(){
		float x = (float) Math.random();
		for(int i = 0; i < P.size(); i++){
			if(x < P.get(i))
				return i;
		}
		return -1;
	}
	public int[] pickTargets(int num){
		int[] ret = new int[num];
		float[] firedP = new float[P.size()];
		float Pcur;
		for(int i = 0; i < num; i++){
			float rand = (float) (Math.random()*(1- firedP[firedP.length -1]));
			for (int j = 0; j < P.size(); j++){
				if (rand < P.get(j) - firedP[j]){
					ret[i] = j;
					if(j == 0){
						Pcur = P.get(j);
					}
					else 
						Pcur = P.get(j) - P.get(j-1);
					for(int k = j; k<firedP.length;k++){
						firedP[k] += Pcur;
					}
					break;
				}
			}
		}
		return ret;
	}
	public void updateP(int[] target, float lambda){
		ArrayList<Float> P_old = new ArrayList<Float>(P);
		float sumTarget = 0;
		for(int i = 0; i<target.length; i++){
			if(target[i] == 0)
				sumTarget += P_old.get(target[i]);
			else
				sumTarget += P_old.get(target[i]) - P_old.get(target[i]-1);
		}
		float sumTargetPrime = sumTarget + (1-sumTarget)*lambda;
		if(target.length == 0){
			return;
		}
		for(int i = 0; i<P.size(); i++){
			boolean contains = false;
			for(int j : target){
				if(j == i)
					contains = true;
			}
			float oldSize;
			if(i == 0)
				oldSize = P_old.get(i);
			else
				oldSize = P_old.get(i) - P_old.get(i-1);
			float newSize;
			if(contains){
				newSize = oldSize+ oldSize*(sumTargetPrime - sumTarget);
				if(i == 0)
					P.set(i, newSize );
				else
					P.set(i, P.get(i-1) + newSize);
			} else {
				newSize = oldSize+ oldSize*(sumTarget-sumTargetPrime);
				if(i == 0)
					P.set(i, newSize );
				else
					P.set(i, P.get(i-1) + newSize);
			}
		}
		float max = P.get(P.size()-1);
		for(int i = 0; i< P.size(); i++){
			P.set(i, P.get(i)*(1/max));
		}
	}
}
