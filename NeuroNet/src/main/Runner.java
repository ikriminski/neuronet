package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
/**
 * Program entry point. First creates a new neural network or re-inflates
 * existing network.
 */

public class Runner {
	/*
	 * Load input files
	 */
	public static String[] files = {"data/EURAUD2013.csv", "data/EURCAD2013.csv",
			"data/EURCHF2013.csv",
			"data/EURGBP2013.csv",
			"data/EURJPY2013.csv",
			"data/EURNZD2013.csv",
	"data/EURUSD2013.csv"};
	/*
	 * Create variable for controlling system between trials
	 */
	Network net;
	LinkedList<float[]> que;
	float[] change;
	float[] controlResult;
	float[] controlProfit;
	int[][][] yearData;
	DataProcessor data;
	boolean invalid;
	/**
	 * Trial parameters
	 */
	public static int BASELINERUNS = 10;
	public static int NUMTRIALS = 3;
	//public static void main(String[] args) {
	//	(new Runner()).run();
	//}
	/*
	 * Start execution
	 */
	public void run(){
		/*
		 * Initialize input processor
		 */
		data = new DataProcessor();
		/*
		 * Make new network code
		 */
		//net = Network.makeNetwork(119, 8, 300, this);
		/*
		 * Unflatten network code.
		 */
		ObjectInputStream in;
		try {
			in = new ObjectInputStream(new FileInputStream("net.dat"));
			net = (Network) in.readObject();
			in.close();
			System.out.println("Opened net");
		} catch (IOException | ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		 * Prep the network.
		 */
		net.setRunner(this);
		//log(net.toString());
		try {
			ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("lastnet.dat"));
			out2.writeObject(net);
			out2.close();
			System.out.println("save last net.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		invalid = false;
		/*
		 * More trial parameters.
		 */
		long startTime = System.nanoTime();
		int numTrials = NUMTRIALS;
		int tickSize = 10;
		int testTick = 1000;
		int duration = 144;
		int startDay = 20130101;
		/*
		 * Load preprocessed data
		 */
		try {
			in = new ObjectInputStream(new FileInputStream("year.dat"));
			yearData = (int[][][]) in.readObject();
			in.close();
		} catch (IOException | ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//yearData = data.processYearData(files, tickSize, testTick, duration);
		/*try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("year.dat"));
			out.writeObject(yearData);
			out.close();
			System.out.println("wrote data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		/*
		 * Parameters for tracking progress
		 */
		float baselinea = 0;
		float baselinep = 0;
		float baselinee = 0;
		long totalE = 0;
		float totalTrainA=0;
		int numTrainA = 0;
		float totalTrainP=0;
		int numTrainP = 0;
		float scoreCounter = 0;
		float sumTrialScore = 0;
		float lastNetScore = 0;
		float[] avgTrialScore = new float[numTrials];
		float[] avgTrialSD = new float[numTrials];
		controlResult = new float[numTrials];
		controlProfit = new float[numTrials];
		//runTestTrial(0);
		System.out.println(controlResult[0]);
		float sumProfit[] = new float[numTrials];
		/*
		 * Run trials
		 */
		for(int i = 0; i< numTrials; i++){
			//net.loosenNetwork(0.998f);
			float SDSum = 0;
			int SDCount = 0;
			for(int m = 0; m < 6; m++){
				for(int d = 0; d<31; d++){
					invalid = false;
					startDay = 20130000 + 100 * (m+1) + d + 1;
					float[] scores = new float[BASELINERUNS];
					for(int j = 0; j<BASELINERUNS; j++){
						runTrial(tickSize, testTick, startDay, duration);
						if(invalid){
							System.out.println("invalid day");
							break;
						}
						float scoreA = scoreA(net.netOut, change);
						if(scoreA == -1){
							j--;
						}else{
							baselinea += scoreA;
							baselinep += scoreP(net.netOut);
							baselinee += net.events.size();
							totalE +=net.events.size();
							scores[j] = scoreA;
						}
						net.flushLog();
						net.flushOutput();
					}

					SDCount++;
					float SD= calculateSD(scores);
					SDSum += SD;
					if(!invalid){
						baselinea = baselinea/BASELINERUNS;
						baselinep = baselinep/BASELINERUNS;
						baselinee = baselinee/BASELINERUNS;
						scoreCounter++;
						sumTrialScore += baselinea;
						boolean trained = false;
						//if(baselinep > 0){
						int attempts = 0;
						while(!trained && attempts < 100){
							attempts++;
							runTrial(tickSize, testTick, startDay, duration);
							float score = scoreA(net.netOut, change);
							if(score < -1){
								score = -1;
							}	


							if(Math.abs(baselinea - score) > SD && score != -1){
								float profit = calculateProfit(net.netOut);
								sumProfit[i] += profit;
								System.out.println("trial num:" + i + " startDay:" + startDay + " score:"+score + " baselinea:" +baselinea + " baselinep:" + baselinep + " baselineE:" + baselinee + " profit:" + profit);
								totalE+= net.events.size();
								totalTrainA += Math.abs(score - baselinea);
								numTrainA++;
								net.teachA((score-baselinea));
								trained = true;
								score = scoreP(net.netOut);
								if(score > baselinep){
									totalTrainP += Math.abs(score - baselinep);
									numTrainP++;
									net.teachP(score - baselinep);
								}
								net.flushLog();
								net.flushOutput();
							}
							net.flushOutput();
							net.flushLog();
							/*} else {
				runTrial(tickSize, testTick, startDay, duration);
				float score = scoreP(net.netOut);
				System.out.println("trial num:" + i + " score:"+score + " baselinea:" +baselinea + " baselinep:" + baselinep + " baselineE:" + baselinee);
				totalE+= net.events.size();
				//if(score > baselinep){
					System.out.println("teaching p");
					totalTrainP += score - baselinep;
					numTrainP++;
					net.teachP(score - baselinep);
				//}
				net.flushLog();
				net.flushOutput();*/
							//}
						}
						baselinea = 0;
						baselinep = 0;
						baselinee = 0;

					}
				}
			}
			avgTrialScore[i] = sumTrialScore/scoreCounter;
			sumTrialScore = 0;
			scoreCounter = 0;
			avgTrialSD[i] = SDSum/SDCount;
			SDSum = 0;
			SDCount = 0;
			runTestTrial(i);
			if(avgTrialScore[i] < lastNetScore){
				i--;
				try {
					in = new ObjectInputStream(new FileInputStream("lastnet.dat"));
					net = (Network) in.readObject();
					in.close();
					System.out.println("Opened last net");
				} catch (IOException | ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				net.setRunner(this);
			} else {
				lastNetScore = avgTrialScore[i];
				try {
					ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("lastnet.dat"));
					out2.writeObject(net);
					out2.close();
					System.out.println("saved net.");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println(net);
		System.out.println("seconds:"+(System.nanoTime() - startTime)/Math.pow(10, 9));
		System.out.println("total events:" + totalE);
		System.out.println("avg events per trial:" + totalE/numTrials);
		System.out.println("avg events per run:" + totalE/(numTrials*31));
		System.out.println("total A training:" + totalTrainA);
		System.out.println("avg A training:" + totalTrainA/numTrainA);
		System.out.println("total P training:" + totalTrainP);
		System.out.println("avg P training:" + totalTrainP/numTrainP);
		System.out.println("train score:" + Arrays.toString(avgTrialScore));
		System.out.println("SD scores:" + Arrays.toString(avgTrialSD));
		System.out.println("control:" + Arrays.toString(controlResult));
		System.out.println("profit:" + Arrays.toString(sumProfit));
		System.out.println("control profit:" + Arrays.toString(controlProfit));
		System.out.println("save net?");
		Scanner scn = new Scanner(System.in);
		String in1 = scn.nextLine();
		if(in1.equals("y")){
			try {
				ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("net.dat"));
				out2.writeObject(net);
				out2.close();
				System.out.println("saved net.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("log net?");
		in1 = scn.nextLine();
		if(in1.equals("y")){
			log(net.toString());
		}
	}
	private void log(String s){
		try {
			PrintWriter out = new PrintWriter("log.txt");
			out.print(s);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private float calculateProfit(float[] out){
		float profit =0;
		for(int i = 0; i<out.length;i++){
			profit += out[i]*(change[i]);
		}
		return profit/out.length;
	}
	private void runTestTrial(int i) {
		int start = 20130701;
		int startDay = 20130201;
		int tickSize = 10;
		int testTick = 1000;
		int duration = 144;
		float sumTrialScore = 0;
		int scoreCounter = 0;
		float sumProfit = 0;
		for(int d = 0; d<31; d++){
			float baselinea = 0;
			invalid = false;
			startDay = start + d;
			float[] scores = new float[15];
			float profit = 0;
			for(int j = 0; j<15; j++){
				runTrial(tickSize, testTick, startDay, duration);
				if(invalid){
					break;
				}
				float scoreA = scoreA(net.netOut, change);
				profit += calculateProfit(net.netOut);
				baselinea += scoreA;
				net.flushLog();
				net.flushOutput();
			}

			if(!invalid){
				profit = profit/15;
				baselinea = baselinea/15;
				scoreCounter++;
				sumTrialScore += baselinea;
				sumProfit += profit;
				//if(baselinep > 0){
				/*} else {
		runTrial(tickSize, testTick, startDay, duration);
		float score = scoreP(net.netOut);
		System.out.println("trial num:" + i + " score:"+score + " baselinea:" +baselinea + " baselinep:" + baselinep + " baselineE:" + baselinee);
		totalE+= net.events.size();
		//if(score > baselinep){
			System.out.println("teaching p");
			totalTrainP += score - baselinep;
			numTrainP++;
			net.teachP(score - baselinep);
		//}
		net.flushLog();
		net.flushOutput();*/
				//}
				baselinea = 0;
			}
		}
		controlResult[i] = sumTrialScore/scoreCounter;
		controlProfit[i] = sumProfit;
	}
	public void askForInput() {
		if(!que.isEmpty()){
			net.setInput(que.pop());
		}

	}
	public void runTrial(int tickSize, int testTick, int startDay, int duration) {		
		que = new LinkedList<float[]>();
		change = new float[files.length + 1];
		int m = ((startDay - 20130000)/100)-1;
		int d = (startDay - 20130000)%100;
		int[][] dout = yearData[31*m + d];
		if(dout == null){
			invalid = true;
		}
		else {
			for(int i = 0; i< dout.length-1; i++){
				float[] in = new float[119];
				for(int k = 0; k< dout[i].length; k++){
					char[] next = Integer.toBinaryString(dout[i][k]).toCharArray();
					for(int l = 0; l<next.length; l++){
						int shift = 17 - next.length;
						if(next[l] == '0')
							in[k*17 + shift + l] = 0;
						else
							in[k*17 + shift + l] = 1;
					}
				}
				que.add(in);

			}
			int[] last = dout[dout.length-2];
			for(int j = 0; j< change.length -1; j++){
				change[j] = ((float) last[j] - dout[dout.length -1][j])/last[j];
			}
			change[change.length -1] = 0f;
			while(net.step());
		}
	}
	public float scoreP(float[] out){
		float sum =0;
		for(float x: out){
			sum += x;
		}
		if(sum <= 23)
			return (sum - 23)/23f;
		if(sum > 23){
			return (23 - sum)/23f;
		}
		return 0;
	}
	public float scoreA(float[] out, float[] change){

		float profit =0;
		float sum =0;
		float max = 0;
		float min = 0;
		for(int i = 0; i<out.length;i++){
			profit += out[i]*(change[i]);
			sum += out[i];
			if(change[i] > max){
				max = change[i];
			}
			if(change[i] < min){
				min = change[i];
			}
		}
		if(sum == 0)
			return -1;
		//System.out.println("sum:"+sum);
		return ((profit - sum*min)/(sum*max - sum*min));

	}
	public float calculateSD(float[] x){
		float u = 0;
		for(float i : x){
			u+= i;
		}
		u = u/x.length;
		float var = 0;
		for(float i : x){
			var += (u-i)*(u-i);
		}
		var = var/(x.length-1);
		return (float) Math.sqrt(var);
	}


}
