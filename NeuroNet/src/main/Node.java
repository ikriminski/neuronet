package main;

import java.io.Serializable;
import java.util.ArrayList;

public class Node implements Serializable{
	Network net;
	Output out;
	Input in;
	String id;
	ArrayList<Node> adjacent;
	static float CFL = 0.002f;
	static float PFL = 0.02f;
	static float CSL = 0.002f;
	static float PSL = 0.02f;
	static float CTL = 0.002f;
	static float PTL = 0.02f;
	static float OL = 0.05f;
	static int MAX_STALL = 10;
	boolean active;
	boolean stalling;
	int stallNum;
	float certaintyf;
	float firingpotential;
	float stallpotential;
	float certaintys;
	float threshold;
	float certaintyt;
	Event e;
	/*
	 * Create new node
	 */
	public Node(){
		adjacent = new ArrayList<Node>();
	}
	void setNetwork(Network n){
		net = n;
	}
	/*
	 * Increase permeability.
	 */
	void loosen(float lambda){
		certaintyf = certaintyf*lambda;
		certaintys = certaintys*lambda;
		certaintyt = certaintyt*lambda;
	}
	/*
	 * Custom sqrt method.
	 */
	float sqrt(float x){
		if(x<0){
			return (float) -Math.sqrt(-x);
		}
		if(x == 0)
			return 0.00001f;
		return (float) Math.sqrt(x);
	}
	public float getThreshold(){
		float rand = (float) (Math.random()*2)-1;
		float th = (float) (threshold + (1-certaintyt)/(sqrt(rand)*2*Math.PI));
		if(th < 0)
			th = 0.00001f;
		return th;
	}
	public int getTimesToFire(){
		float rand = (float) (Math.random()*2)-1;
		int numFired = (int) Math.round(firingpotential + (1-certaintyf)/(sqrt(rand)*2*Math.PI));
		if(numFired < 0)
			numFired = 0;
		return numFired;
	}
	public int getStallTime(){
		float rand = (float) (Math.random()*2)-1;
		int stallTime = (int) Math.round(stallpotential + (1-certaintys)/(sqrt(rand)*2*Math.PI));
		if(stallTime <= 0){
			stallTime = 1;
		}
		if(stallTime > MAX_STALL){
			stallTime = MAX_STALL;
		}
		return stallTime;
	}
	public void modifyFiringPotential(int lastPotential, float lambda){
		float deltaX = firingpotential*certaintyf/Math.abs(firingpotential - lastPotential);
		firingpotential += (lastPotential - firingpotential)*lambda*(1-certaintyf)*PFL;
		//certaintyf += (1-certaintyf)*lambda*CFL;
		//certaintys += (firingpotential*(1-certaintyf))/(lastPotential);
		float notC = 1-certaintyf;
		if(deltaX == Float.NaN){
			deltaX = 0.001f;
		}
		if(deltaX > 1){
			notC += ((notC)/deltaX - notC)*lambda*CFL;
			certaintyf = 1-notC;
		} else {
			certaintyf += (1-deltaX)*notC*lambda*CFL;
		}
	}
	public void modifyStallPotential(int lastPotential, float lambda){
		float deltaX = stallpotential*certaintys/Math.abs(stallpotential - lastPotential);
		stallpotential += (lastPotential - stallpotential)*lambda*(1-certaintys)*PSL;
		//certaintys += (1-certaintys)*lambda*CSL;
		float notC = 1-certaintys;
		if(deltaX == Float.NaN){
			deltaX = 0.001f;
		}
		if(deltaX > 1){
			notC += ((notC)/deltaX - notC)*lambda*CSL;
			certaintys = 1-notC;
		} else {
			certaintys += (1-deltaX)*notC*lambda*CSL;
		}
	}
	public void modifyThreshold(float lastIn, float lambda){
		threshold += (lastIn-threshold)*lambda*PTL;
		if (threshold < 0){
			threshold = 0;
		}
		certaintyt += (1-certaintyt)*lambda*CTL;
		//certaintyt += (threshold + (1-certaintyt))/(threshold - lastIn);
	}
	public void modifyOutput(int[] lastTarget, float lambda){
		out.updateP(lastTarget, lambda*OL);
	}
	void setAdjacent(ArrayList<Node> adj){
		for (Node n:adj){
			adjacent.add(n);
		}
	}
	
	void setId(String name){
		id = name;
	}
	void setInput(Input nin){
		in = nin;
	}
	
	void setOutput(Output nout){
		out = nout;
	}
	
	void print(){
		System.out.println("Node:");
		System.out.println("  Input:");
		in.print();
		System.out.println("  Output:");
		out.print();	
	}
	
	void readInput(){
		if(stalling){
			stallNum--;
			if(stallNum <= 1){
				stalling = false;
				int curFire = getTimesToFire();
				fire(curFire);
				in.zero();
				makeInactive();
			} 
		} else {
			float curIn = in.getInput();
			if(curIn/getThreshold() > Math.random()){
				int curStall = getStallTime();
				stallNum = curStall;
				e = new Event(curStall);
				e.input = curIn;
				if(curStall > 1){
					curStall--;
					stalling = true;
				} else {
					int curFire = getTimesToFire();
					fire(curFire);
					in.zero();
					makeInactive();
				}
			} else {
				in.step();
				if(in.getInput() == 0){
					makeInactive();
				}
			}					
		}
	}
	
	void fire(int num){
		if(num > out.P.size()){
			num = out.P.size();
		}
		int[] outx = out.pickTargets(num);
		for(int i = 0; i < num; i++){
			adjacent.get(outx[i]).receiveInput(1);
		}
		e.targets=outx;
		e.n = this;
		net.log(e);
	}
	
	void receiveInput(float d){
		in.recieveInput(d);
		if(in.getInput()> 0){
			makeActive();
		}
	}

	void makeActive() {
		if(!active){
			net.addActive(this);	
			active = true;
		}
	}
	
	void makeInactive() {
		if(active){
			net.removeActive(this);
			active = false;
		}
	}
	@Override
	public String toString() {
		return "Node [id=" + id + ", certaintyf=" + certaintyf + ", firingpotential=" + firingpotential
				+ ", stallpotential=" + stallpotential + ", certaintys=" + certaintys + ", threshold=" + threshold
				+ ", out.P=" + out.P + "]\n" ;
	}
	
	

}
