package testing;

import java.util.ArrayList;

import bio.BioNet;
import bio.Codon;

public class Tester1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//System.out.println(Integer.toBinaryString(1));
		ArrayList<Codon> test = new ArrayList<Codon>();
		for(int i = 0; i<12; i++) {
			test.add(new Codon(Codon.TYPE.NEURON, 0 , 0 ));
		}
		double bestScore = 0;
		ArrayList<Codon> bestNet = test;
		for(int h = 0; h<100000; h++){
			BioNet testnet = new BioNet(bestNet);
			testnet.buildNet();
			testnet = new BioNet(testnet.mutate());
			testnet.buildNet();
			//System.out.println(testnet.neurons.size());
			String[] outputText = new String[]{"1", "2", "3", "4", "5", "6", "7", "8"};
			testnet.createInputArray(4);
			testnet.createOutputArray(8, outputText);

			
			double finalTestScore = 0;
			for(int i = 1; i <=8; i++){
				String input = Integer.toBinaryString(i);
				//System.out.println(input);
				for(int j = 0; j< input.length(); j++){
					if(input.charAt(j) == '1'){
						//System.out.println("setteing active:" + (4-input.length() + j));
						testnet.inputNeurons.get(((4-input.length()) + j)).setActive(10);
					}
				}
				//int loopCounter = 0;
				do{
					testnet.step();
					//loopCounter++;
				} while(testnet.hasActiveNeurons());
				//System.out.println("loopcounter:"+loopCounter);
				int outputCount = 0;
				double score = 0;
				double finalRunScore = 0;
				String output = testnet.outputBuffer;
				//System.out.println(output);
				for(int j = 0;output!= null && j< output.length() ; j++){
					if(output.charAt(j) == (char) (i+48)){
						score++;
					}
					outputCount++;
				}
				if(outputCount != 0)
					finalRunScore = score/outputCount;
				//System.out.println(score);
				finalTestScore += finalRunScore;
				testnet.flushOutputBuffer();
				testnet.reset();
			}

			if(finalTestScore > bestScore){
				bestScore = finalTestScore;
				bestNet = testnet.getDNA();
			}
		}
		System.out.println("best score:" + bestScore + " bestDna:" + bestNet);
		System.out.println((char) 48);
	}

}
